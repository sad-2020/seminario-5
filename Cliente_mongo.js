const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express');
const routeMongo = express();
const request = require('request');
const urlreg ='http://localhost:5000/register/mongodb/1/7000';

const registro = function(urlreg){


    request.put(urlreg , function(err, res, body) {
        console.log(body)
    });
}
// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'myproject';

// Create a new MongoClient
const client = new MongoClient(url, {useNewUrlParser: true }, { useUnifiedTopology: true });

registro(urlreg);

// Use connect method to connect to the Server
client.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);

    // insertDocuments(db, function() {
    //findDocuments(db,function() {
    ///client.close();
    // });
    // });


});

routeMongo.get('/stock/:descripcion', function(req, res){
    const db = client.db(dbName);
    //
    const desc = req.params.descripcion;

    findDocuments(db,desc,function(docs) {
        var resp = docs[0].stock;
        console.log(resp);
        res.json(resp);

        //client.close();
    });
});




const insertDocuments = function(db, callback) {
    // Get the documents collection
    // collection = db.collection('documents');
    const collection = db.collection('products');
    // Insert some documents
    collection.insertMany([
            {cod : 1,desc:'palos',stock:0}, {cod : 2,desc:'hierros',stock:10}, {cod : 3,desc:'muelles',stock:5}
        ]
        , function(err, result) {
            assert.equal(err, null);
            assert.equal(3, result.result.n);
            assert.equal(3, result.ops.length);
            console.log("Inserted 3 documents into the collection");
            callback(result);
        });
}

const findDocuments = function(db, desc, callback) {
    // Get the documents collection

    const collection = db.collection('products');
    // Find some documents
    collection.find({desc:desc}).toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs)
        callback(docs);
        //return docs;
    });
}
routeMongo.use((error, req, res, next) => {
    res.status(error.status || 500);
    // Log out the error to the console
    //log.error(error);
    return res.json({
        error: {
            message: error.message,
        },
    });
});
routeMongo.listen(7000);